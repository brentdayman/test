module.exports = function(grunt) {
  //load grunt tasks
  require('load-grunt-tasks')(grunt);
 
  //time how long task take
  require('time-grunt')(grunt);

  // Configurable paths
  var config = {
      dev: 'dev', //development folder: <%= config.dev %>
      build: 'build' //build/dist folder: <%= config.build %>
  };

  // define the configuration
  grunt.initConfig({
    
    // read the package.json to install correct plugins
    pkg: grunt.file.readJSON('package.json'),
 
    // Project settings
    config: config,

    copy: {
      build: {
        cwd: '<%= config.dev %>',
        src: [ 
          // copy html
          '*.html', 

          // copy icons
          '*.{ico,png,svg}',
          
          // except all SCSS
          '!SCSS/*',

          //jquery
          'js/jquery.min.js',

          //all fonts
          'fonts/*',

          //images
          'images/*.{jpg,png,svg}',
          
          //ignore cruft
          '!**/*.db', 
        ],
        dest: '<%= config.build %>',
        expand: true
      },
      dev: {  //only copy .HTML, plugin.js, fonts
        cwd: '<%= config.dev %>',
        src: [ 
          '*.html', 
          'fonts/*', 
          'js/jquery.min.js',
          'images/*.{jpg,png,svg}',
          '!**/*.db', 
          '*.{ico,png,svg,jpg}', 
          'images/*.{png,svg,jpg}'],
        dest: '<%= config.build %>',
        expand: true
      }
    },

    clean: {
        build: {
            files: [{
                dot: true,
                src: [
                    '<%= config.build %>/*'
                ]
            }]
        }
    },

    modernizr: {
      build: {
          "devFile" : "dev/js/modernizr.js",

          "outputFile" : "build/js/modernizr.js",

          "extra" : {
              "shiv" : false,
              "printshiv" : true,
              "load" : true,
              "mq" : false,
              "cssclasses" : true
          },

          "extensibility" : {
              "addtest" : false,
              "prefixed" : false,
              "teststyles" : true,
              "testprops" : true,
              "testallprops" : true,
              "hasevents" : false,
              "prefixes" : true,
              "domprefixes" : true
          },

          "uglify" : true,
          "parseFiles" : true
      }
    },

    sass: {
      dist: {
        options: {
          style: 'compact'
        },
        files: {
          '<%= config.dev %>/SCSS/process/main.css': '<%= config.dev %>/SCSS/main.scss'
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 version', 'ie 8']
      },
      multiple_files: {
          expand: true,
          //flatten: true,
          cwd: '<%= config.dev %>/SCSS/process/',
          src:'*.css',
          dest:'<%= config.build %>/css/' 
      }
    },

    cssmin: {
      minify: {
        expand: true,
        cwd: '<%= config.build %>/css/',
        src: ['*.css', '!*.min.css'],
        dest: '<%= config.build %>/css/', 
        ext: '.min.css'
      }
    },
 
    jshint: {
      //just checking own JS
      beforeconcat: ['<%= config.dev %>/js/client_ui.js']
    },
 
    concat: {
      dist: {
        src: [
          '<%= config.dev %>/js/plugins.js',
          '<%= config.dev %>/js/client_ui.js'
        ],
        dest: '<%= config.build %>/js/client_ui.js'
      }
    },

    uglify: {
      build: {
        src: '<%= config.build %>/js/client_ui.js',
        dest: '<%= config.build %>/js/client_ui.min.js'
      }
    },

    express: {
      all: {
        options: {
          port: 9000,
          hostname: '0.0.0.0',
          bases: '<%= config.build %>',
          livereload: true
        }
      }
    },
    open: {
      all: {
        path: 'http://localhost:<%= express.all.options.port %>/index.html'
      }
    },
    watch: {
      configFiles: {
        files: [ 'Gruntfile.js', 'config/*.js' ],
        options: {
          reload: true
        }
      },
      all: {
        // Replace with whatever file you want to trigger the update from
        // Either as a String for a single entry 
        // or an Array of String for multiple entries
        // You can use globing patterns like `css/**/*.css`
        // See https://github.com/gruntjs/grunt-contrib-watch#files
        files: [
          '<%= config.build %>/*.html',
          '<%= config.build %>/*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: true
        }
      },
      scripts: {
        files: ['<%= config.dev %>/js/*.js'],
        tasks: ['concat', 'jshint', 'uglify'],
        options: {
          spawn: false,
          livereload: true
        }
      },
      css: {
        files: ['<%= config.dev %>/scss/*.scss'],
        tasks: ['sass', 'autoprefixer', 'cssmin'],
        options: {
          spawn: false,
          livereload: true
        }
      },
      copy: {
        //files: ['<%= config.dev %>/**', '!<%= config.dev %>/SCSS/**', '!<%= config.dev %>/js/**', '!<%= config.dev %>/css/**', '!**/*.db', '!images/', '!js-customise/', '!fonts/' ],
        files:['<%= config.dev %>/*.html', '<%= config.dev %>/*.png', '<%= config.dev %>/*.svg', '<%= config.dev %>/js/plugins.js', '<%= config.dev %>/images/*'],
        tasks: ['newer:copy:dev'],
        options: {
          spawn: false,
          livereload: true
        }
      } 
    },
    svgmin: {
      options: {
          plugins: [
              { removeViewBox: false }, 
              { removeUselessStrokeAndFill: false }
          ]
      },
      dist: {
        expand: true,
            cwd: '<%= config.dev %>/svg/raw',
            src: ['*.svg'],
            dest: '<%= config.dev %>/svg/compressed'
      }
    },
    grunticon: { //makes SVG icons into a CSS file
        myIcons: {
          files: [{
            expand: true,
            cwd: '<%= config.dev %>/svg/compressed',
            src: ['*.svg'],
            dest: '<%= config.dev %>/svg/output'
          }],
          options: {
            cssprefix: '.icon-'
          }
        }
      }
  });
 
 // run after build
  grunt.registerTask('dev', [
    'express',
    'open',
    'watch',
  ]);

  // run first
  grunt.registerTask('build', [
    'clean:build',
    'jshint',
    'concat', 
    'uglify',
    'sass', 
    'autoprefixer', 
    'cssmin',
    'copy:build',
    'modernizr'
  ]);

  //compress icons - still need to run them through: http://grumpicon.com/
  grunt.registerTask('svg', [
    'svgmin',
    'grunticon'

  ]);

  grunt.registerTask('default', ['dev']);

};