# README #

Origin Test

##Assumptions##
* Non-JavaScript equivalent form functionality was not included in the construction.
* No attempt has been made to correct form accessibility issues (including current keyboard traps, hidden labels, missing bypass blocks, colour contrast etc).
* No indication of mandatory fields was shown on visuals - have to complete all fields.
* No hover/active/visited states in design (using Origin corporate site as a guide).
* Large '#' in disclaimer for Desktop version a typo? Corrected to superscript version.


##Completed##
* Basic functionality, including:
	* light box pop up enquiry form,
	* form fields validation,
	* spambot random equation, and
	* display thank you message once submitted with user's details showing.
* Basic responsive layout with 765px breakpoint. 
* CSS3 used with fallbacks (including Grunt auto-prefixer and Modernizr).
* JavaScript Lint.
* W3C compliance.
* HTML5 Semantic Structure, with some SEO meta tag placeholders.
* JavaScript Task runner - details below.

##Not completed - due to timing##
* Responsive images fallback - only logotype has Non-SVG version.
* Browser testing - only rudimentary testing completed, including:
	* Chrome Mac latest, 
	* Safari Mac latest, 
	* FireFox Mac latest,
	* iPhone 5 iOS8,
	* Nexus 7 Chrome latest,
	* iPad Mini Safari iOS7, and
	* iPad Mini Safari iOS8.
* GA tracking excluded
* Accessibility testing excluded - including compliance to WCAG 2.0 Level AA
* Phone number validation
* No close button on modal dialogue box - forcing the user to submit. Should have:
	* close button, 
	* escape key closes form, and 
	* clicking on the background closes form.

##Contains##
* Read me file
* Gruntfile
* `Dev\` development files
* `Build\` final compressed files




---

---

## Workflow details ##

### Dev environment ###

* Clone (requires [GIT](#dependancies)) the latest Framework repo from [BitBucket](https://bitbucket.org/brentdayman/test)

```
git clone git@bitbucket.org:brentdayman/test.git
```
* Install [dependancies](#dependancies) (Requires Node.js and Grunt)

* Change directory to installed folder `test` and install Grunt CLI, Grunt and Grunt modules


```
//cd test

//Install the CLI
npm install -g grunt-cli

//Add Grunt
npm install grunt --save-dev

//Add Modules
npm install

```

Initial build to create set up:

```
grunt build
```

Start up dev environment and web site:

```
grunt dev
```

This will:

* run jshint over custom js,
* concat js files, 
* compress js
* compile sass, 
* add autoprefixer,
* compress css 
* copy over files to /dev
* launch web browser (on saving HTML files, CSS and JS auto-reloads)


### Files to edit ###
*Note: do not add files to build directory - these will be deleted*
#### SCSS ####
Edit the files: 

* dev/scss/*

Will output CSS files to /build

#### JS ####
Edit the files:

* dev/js/client_ui.js (anything client specific)

Will output concat file to /build

#### HTML ####
Edit the HTML files in /dev

Will be automatically copied to /build


### Testing ###
Files can be tested on any device during development by going to http://ip-address:8000.

Note: To find out your machine's IP, run `ifconfig` in the command line and look for the IP next to inet addr. On Windows, you run ipconfig and look for IPv4 Address.


### Final Build ###
To prep the final files:

```
grunt build
```

This will:

* remove all working files from build,
* run jshint over custom js,
* concat js files, 
* compress js
* compile sass, 
* add autoprefixer,
* compress css 
* copy over files to /dev
* add project specific modernizr

---


## Further information

### Framework Dependancies Installation  
#### Git install ####
Download the installer [here](http://msysgit.github.io) for Windows
##### Git Key generation #####
* Generating SSH Keys for [Git Hub](https://help.github.com/articles/generating-ssh-keys), or
* Set up SSH for Git - [BitBucket Guide](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)

#### Node install ####
Download the Node.js source or a pre-built installer [here](http://nodejs.org/download/)



### Grunt information ###
http://24ways.org/2013/grunt-is-not-weird-and-hard/
http://www.html5rocks.com/en/tutorials/tooling/supercharging-your-gruntfile/
http://danburzo.ro/grunt/chapters/front/
http://rhumaric.com/2013/07/renewing-the-grunt-livereload-magic/

#### Grunt SVG conversion ####
http://www.wearejh.com/design/svg-icon-sprites-optimized-workflow/

---

### Who do I talk to? ###
[brent@dayman.com.au](brent@dayman.com.au)