// ==========================================================================
//    Project/Client specific
// ==========================================================================

var client_site = {
    init: function () {
        this.modalDialogueBox();
        this.activateModalDialogueBox();

        //this.echoVersionNumber();
    },
    // echoVersionNumber: function () {
    //     var jsVersion = "version 0.5 - cleanout";
    //     console.log(jsVersion);
    // },

    modalDialogueBox: function () {
        //insert lighbox background
        $("body").prepend("<div class='lightbox__screen'></div>");

        // Forms
        var $oForm = $(".oform__signup"),
            $oFormThanks = $('.oform__thankyou'),
            // set up the maths question
            robotLS = client_site.getRandomInt(1, 5),
            robotRS = client_site.getRandomInt(1, 5),
            // robotSum = robotLS + robotRS,
            $robotInput = $('#oform_robot'),
            robotPlaceholder = robotLS + " + " + robotRS;

        // console.log(robotLS + " + " + robotRS + " = " + robotSum);
        
        // this is a bad idea as there is no placeholder fallback
        // but doing this for sake of time
        $robotInput.attr("placeholder", robotPlaceholder);

        $.validator.addMethod("verify", function (value, element, params) {
            return this.optional(element) || parseInt(value, 10) === params[0] + params[1];
        }, jQuery.validator.format("Please add {0} + {1}"));

        $oForm.validate({
            rules: {
                oform_name: "required",
                oform_email: {
                    required: true,
                    email: true
                },
                oform_phone: "required",
                oform_robot: {
                    required: true,
                    verify: [robotLS, robotRS]
                }
            },
            messages: {
                oform_name: "Please enter your name",
                oform_email: "Please enter a valid email address",
                oform_phone: "Please enter your phone number"
            },
            errorClass: "oform__input--invalid",
            submitHandler: function (form) {
                //hide form
                $oForm.addClass('oform__signup--inactive');
                $oFormThanks.addClass('oform__thankyou--active');
                client_site.showFormValues();
            }
        });
    },

    activateModalDialogueBox: function () {
        //open/close DialogueBox
        var $opener = $('a[data-lightbox-open="true"]'),
            $closer = $('a[data-lightbox-close="true"]');

        $opener.on('click', function (e) {
            $('body').addClass('lightbox--active');
            e.preventDefault();
        });
        $closer.on('click', function (e) {
            $('body').removeClass('lightbox--active');
            e.preventDefault();
        });
    },

    showFormValues: function () {
        //simple: $(".oform__signup").serialize();
        $('.oform_name--value').text($('#oform_name').val());
        $('.oform_email--value').text($('#oform_email').val());
        $('.oform_phone--value').text($('#oform_phone').val());
        $('.oform_robot--value').text($('#oform_robot').val());
    },

    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
};

client_site.init();